<?php get_header(); ?>
		<div id="mainHead">
			<div id="mainHeadContent">
				<div id="timerBox">
					<span id="ato">あと</span><span id="timer"></span>
				</div>
			<script>$('#timer').yycountdown({endDateTime:'2017/08/31 00:00:00',unit: {d: '日', h: '時間', m: '分', s: '秒'},complete:function(){m = (new Date( )).getMonth( ) + 1;d = (new Date( )).getDate( );
			if ((m ==  8) && (d == 31)) document.write("1日目");else if ((m ==  9) && (d == 1)) document.write("2日目");else if ((m ==  9) && (d == 2)) document.write("3日目");}});</script>
			<div id="topNavBox">
				<div class="topNav col-1-3"><a href="./schedule"><div class="table"><div>企画・スケジュール</div></div></a></div>
				<div class="topNav col-1-3"><a href="./information"><div class="table"><div>一般の方へのご案内</div></div></a></div>
				<div class="topNav col-1-3"><a href="./students"><div class="table"><div>生徒向けページ</div></div></a></div>
			</div>
		</div>
	</div>
	<div id="mainContentBox">
		<div id="mainContent">
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
				<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>
				</div>
				</div>
				<?php endwhile; endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
