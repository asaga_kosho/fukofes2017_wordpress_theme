<div id="mainHead">
  <div id="mainHeadContent">
    <div id="mainHeadTitleBox">
      <div style="display: table;width: 100%;height:100%;">
        <div style="display: table-cell;vertical-align: middle;text-align: center;">
          <h1><?php wp_title ( '', true,'right' ); ?></h1>
        </div>
      </div>
    </div>
    <div id="topNavBox">
    <div class="topNav col-1-3"><a href="<?php echo home_url(); ?>/schedule"><div class="table"><div>企画・スケジュール</div></div></a></div>
    <div class="topNav col-1-3"><a href="<?php echo home_url(); ?>/information"><div class="table"><div>一般の方へのご案内</div></div></a></div>
    <div class="topNav col-1-3"><a href="<?php echo home_url(); ?>/students"><div class="table"><div>生徒向けページ</div></div></a></div>
  </div>
</div>
</div>
