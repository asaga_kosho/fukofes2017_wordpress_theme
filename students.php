<?php
/*
Template Name: students
*/
?>
<?php get_header(); ?>
		<?php get_template_part('mainHead'); ?>
	<div id="mainContentBox">
		<div id="mainContent">
      <a href="<?php echo home_url(); ?>">ホーム</a>--><?php the_title(); ?>
      <?php if(have_posts()): while(have_posts()): the_post(); ?>
        <?php the_content(); ?>

        <?php endwhile; endif; ?>
      <h2>お知らせ一覧</h2>
			<?php $paged = get_query_var('paged'); ?>
    <?php query_posts("posts_per_page=50&paged=$paged"); ?>
    <ul style="padding-left: 10px;">
    <?php if (have_posts()) : while(have_posts()) : the_post(); ?>

        <li style="list-style-type: none;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>（<?php the_author(); ?>）</li>


    <?php endwhile; ?>
    <?php else: ?>
      <p>現在お知らせはありません。</p>
    <?php endif; ?>
  </ul>

	</div>
</div>
<?php get_footer(); ?>
