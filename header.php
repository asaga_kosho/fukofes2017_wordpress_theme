<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta name="keywords" content="附高祭,附高天王寺,大阪教育大学附属高等学校天王寺校舎,附属天王寺,飛彩">
	<meta name="description" content="大教大附高天王寺の附高祭(文化祭)広報サイトです。">
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:title" content="<?php wp_title ( '|', true,'right' ); ?><?php bloginfo('name'); ?>" />
	<meta name="twitter:description" content="大教大附高天王寺の附高祭(文化祭)広報サイトです。" />
	<!-- <meta name="twitter:image" content="http://fukosai.info/assets/images/onemblemwhite.png" /> -->
	<meta name="twitter:url" content="http://fukosai.info/" />
	<title><?php wp_title ( '|', true,'right' ); ?><?php bloginfo('name'); ?></title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/assets/stylesheets/main.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/assets/stylesheets/simplegrid.css">
	<style media="screen">
	<?php $array = array( "#d77","#d95","#db4","#dd3","#bd4","#9d5","#7c6","#5c8","#5bb","#5ac","#59d","#68d","#87d","#95d","#b5b","#d69");?>
	div#timerBox, div#mainHeadTitleBox {
		color: <?php $key = array_rand($array);echo $array[$key];?>;

	}
	</style>
</head>
<body>
  <?php wp_head(); ?>
	<div id="main">
		<header>
			<div>
				<a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo.svg" alt="" id="headerLogo"></a>
			</div>
		</header>
