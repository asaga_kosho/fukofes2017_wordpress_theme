<?php
/*
Template Name: students
*/
?>
<?php get_header(); ?>
		<?php get_template_part('mainHead'); ?>
	<div id="mainContentBox">
		<div id="mainContent">
      <?php if(have_posts()): while(have_posts()): the_post(); ?>
				<a href="<?php echo home_url(); ?>">ホーム</a>--><a href="<?php echo home_url(); ?>/students">生徒向けページ</a>--><?php the_title(); ?>
        <h2><?php the_title(); ?></h2>
				<?php the_content(); ?>


				<?php endwhile; endif; ?>
		</div>
	</div>
<?php get_footer(); ?>
