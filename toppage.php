<?php
/*
Template Name: toppage
*/
?>
<?php get_header(); ?>
		<div id="mainHead">
			<div id="mainHeadContent">
				<div id="timerBox" style="display:table;width:100%;">
					<div style="display:table-cell;vertical-align:middle;text-align:center;">
						<?php
						$today = time();
						$cdto = strtotime("20170831");
						$int = $cdto - $today;
						$day = ceil($int/24/60/60);
						if ($day > 0) {
						  echo "<span id='ato'>あと</span><br><span id='timer'>{$day}日</span>";
						}elseif ($day == 0) {
						  echo "<span id='timer'>附高祭１日目</span>";
						}elseif ($day == -1) {
						  echo "<span id='timer'>附高祭２日目</span>";
						}elseif ($day == -2) {
						  echo "<span id='timer'>附高祭３日目</span>";
						}else {
						  echo "<span id='timer'>終了しました<span>";
						}
						?>

					</div>

				</div>

			<div id="topNavBox">
				<div class="topNav col-1-3"><a href="<?php echo home_url(); ?>/schedule"><div class="table"><div>企画・スケジュール</div></div></a></div>
				<div class="topNav col-1-3"><a href="<?php echo home_url(); ?>/information"><div class="table"><div>一般の方へのご案内</div></div></a></div>
				<div class="topNav col-1-3"><a href="<?php echo home_url(); ?>/students"><div class="table"><div>生徒向けページ</div></div></a></div>
			</div>
		</div>
	</div>
	<div id="mainContentBox">
		<div id="mainContent">
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
				<?php the_content(); ?>
				</div>
				</div>
				<?php endwhile; endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
